# Go Service Bus (GSB)

# Vision
1. More impact, More quickly, Less code.

## Principles
1. Keep it Simple
2. You aren't gonna need it
3. Design for Evolution
4. Least Astonishment
5. Avoid Premature Optimisation

## Front of Mind
1. What are you doing
2. What can go wrong

## Rules
1. Anything that fails during startup / initialisation should fail immediatley with descriptive STDOUT.
2. Anything that fails during execution should return an error and be processed by the system, to allow continuous operation.
3. Handlers must implement Handler interface
   1. Handlers can optionally implement an Init() method, with optional return error.
4. All user space code execution is initiated by receiving a message.

## Guidelines
1. All external resources are initialised and validated on start up, then provided on execution as Application Resources.

## Rationale
1. Try to raise the level of infrastructure.

2. The infrastructure,
   1. takes care of starting, committing and rolling back transactions.
   2. runs all code within the context of a message.
   3. provides automatic retry if the code errors
   4. moves failed messages to the error queue

## Description
The backbone of GSB is a message queue.

All work is initiated by pulling messages from this message queue.

If all external dependencies, eg, queues, databases, etc are provided as Application Resources, gsb will manage all transactions, meaning the message then provides a transaction boundary.

In this model, a number of options present when errors occur while processing a message.
One, they can be retried. For example, their could be a network glitch which has corrected itself by the time the message is retried.
Two, for persistent errors, the message can be moved to error queue. For example, a database may be down. In this case, the database can be restarted, and the message sent back for processing.

## Variables

1. NAME
2. RETRIES
3. ERROR_Q
4. AUDIT_Q
5. MQ
6. GSB_
7. GSBMON_
8. VERBOSE

### Backbone Message Queue

MQ=type://[username:password@]host/queue_name

1. beanstalk
2. sqs
3. inmemq

### Application Resources

GSB_[name]=<type>://[username:password@]<host>/<path>

All external resources are initialised and validated on start up, then provided on execution.


1. DynamoDb
   1. dynamodb
2. FileSystem
   1. s3
   2. fs
3. KeyValue
   1. dir
   2. redis
4. Queue
   1. beanstalk
   2. sqs
   3. inmemq

### AWS Specific

1. AWS_ENDPOINT
2. AWS_REGION

### Monitors

GSBMON_[name]=<type>://[username:password@]<host>/<path>

A monitor will keep checking the end point for incoming messages. If found, it pull the message,
wrap it in an envelope, and post it to the backbone mq for processing.
The name of the message will be the name portion indicated in the URI above.

1. dir
2. queue

## COMMANDS
To enable the ability to configure the running system, gsb will process commands coming in via the backbone MQ,

1. GSB_VERBOSE_ON
2. GSB_VERBOSE_OFF
3. GSB_AUDIT_ON
4. GSB_AUDIT_OFF

## References

1. [8 Fallacies of Distributed Computing](https://en.wikipedia.org/wiki/Fallacies_of_distributed_computing)
2. [12 Factor App](https://12factor.net/)
3. [GSB](https://gitlab.com/guyirvine/gsb/)
