package gsb

import (
	"os"
	"testing"
)

func setupForName(name string) *Host {
	ResetMqInMemory()

	os.Setenv("NAME", name)
	os.Setenv("MQ", "inmem://localhost/test")

	h := NewHost()

	return h
}

func TestSetName(t *testing.T) {
	h := setupForName("TESTING")

	if h.Name != "TESTING" {
		t.Fatalf("TestSetName. expected: TESTING, actual: %s", h.Name)
	}
}

func TestDefaultName(t *testing.T) {
	h := setupForName("")

	if h.Name != "gsb" {
		t.Fatalf("TestSetName. expected: gsb, actual: %s", h.Name)
	}
}
