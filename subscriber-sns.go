package gsb

import (
	"context"
	"log"
	"net/url"
	"encoding/json"

	"github.com/aws/aws-sdk-go-v2/service/sns"

	"gitlab.com/guyirvine/gsb/appres"
)

// SnsMessage represents an incoming aws SNS message
type SnsMessage struct {
	Message string
}

// ParseIncomingSnsMessage populates the SnsMessage struct from the payload
func ParseIncomingSnsMessage(payload string) (*SnsMessage, error) {
	var s SnsMessage
	if err := json.Unmarshal([]byte(payload), &s); err != nil {
		return nil, err
	}

	return &s, nil
}

type subscriberSns struct {
	url           *url.URL
	name          string
}

func newSubscriberSns(url *url.URL) *subscriberSns {
	sns := new(subscriberSns)
	sns.url = url
	if url.Path == "" {
		log.Fatalf("Must provide a queue name for newSubscriberSns. Environment Variable, MQ=%s\n", url.String())
	}
	sns.name = url.Path[1:]

	return sns
}

func (s *subscriberSns) Name() string {
	return s.name
}

func (s *subscriberSns) Subscribe() (error) {
	cfg := appres.NewAwsConfig()

	client := sns.NewFromConfig(cfg)

	endpoint := s.url.String()
	protocol := s.url.Scheme
	topicARN := ""

	result, err := client.Subscribe(context.TODO(), &sns.SubscribeInput{
		Endpoint:              &endpoint,
		Protocol:              &protocol,
		ReturnSubscriptionArn: true, // Return the ARN, even if user has yet to confirm
		TopicArn:              &topicARN,
	})

	log.Printf("*result.SubscriptionArn: %s\n", *result.SubscriptionArn)

	if err != nil {
		return err
	}

	return nil
}

// Begin Transaction
func (s *subscriberSns) Begin() error {
	return nil
}

// Commit Transaction
func (s *subscriberSns) Commit() error {
	return nil
}

// Rollback Transaction
func (s *subscriberSns) Rollback() error {
	return nil
}
