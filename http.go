package gsb

import (
  "net/http"
	"fmt"
	"log"
  "os"
)

func startHTTPServer() {
  address := os.Getenv("HTTP_ADDRESS")
	if address == "" {
		address = ":8080"
	}

  http.HandleFunc("/health", func(res http.ResponseWriter, req *http.Request) {
    fmt.Fprintf(res, "Ok")
  })

  go func() {
    log.Fatal(http.ListenAndServe(address, nil))
  }()

  log.Printf("HTTP Server started at address: %s", address)
}
