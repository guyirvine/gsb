package gsb

// TestEnvelope is available to be used for user space tests
type TestEnvelope struct {
  name string
  payload string
}

// TestBus is available to be used for user space tests
type TestBus struct {
  SendList []TestEnvelope
}

// Send is the mock for Sending Messages
func (tb *TestBus) Send(name string, payload string) error {
  tb.SendList = append(tb.SendList, TestEnvelope{name, payload})
  return nil
}

// AppVerboseLog is a wrapper for verbose logging
func (tb *TestBus) AppVerboseLog(_ string) {
}
