package gsb

import (
	"errors"
	"fmt"
	"os"
	"testing"

	"gitlab.com/guyirvine/gsb/appres"
)

type ReplayTestingHandler struct {
	replayRunCount int
	replaySuccess  bool
}

func (ha *ReplayTestingHandler) Name() string {
	return "ReplayTestingHandler"
}

// This will pass after two retries, meaning it will run three times
func (ha *ReplayTestingHandler) Handle(_ string) error {
	ha.replayRunCount = ha.replayRunCount + 1
	if ha.replayRunCount < 3 {
		msg := fmt.Sprintf("Generated Error. %d", ha.replayRunCount)
		return errors.New(msg)
	}

	ha.replaySuccess = true
	return nil
}

func setupHost(retries string) *Host {
	ResetMqInMemory()
	appres.ResetQueueInMemory()

	os.Setenv("NAME", "TESTING")
	os.Setenv("MQ", "inmem://localhost/test")
	os.Setenv("RETRIES", retries)

	h := NewHost()

	return h
}

func setupHandler(h *Host) *ReplayTestingHandler {
	ha := new(ReplayTestingHandler)
	h.AddHandler(ha)

	h.Send("ReplayTesting", "Test Message")

	ha.replayRunCount = 0
	ha.replaySuccess = false

	return ha
}

func checkenvelopeErrorCount(t *testing.T, payload string, expected int) {
	env, err := unmarshalEnvelope([]byte(payload))
	if err != nil {
		t.Fatalf("TestError. %v", err)
	}
	if len(env.Errors) != expected {
		t.Fatalf("TestError. len(env.Errors). expected: %d, actual: %d.", expected, len(env.Errors))
	}
}

func TestReplay(t *testing.T) {
	h := setupHost("0")
	ha := setupHandler(h)

	errQ := appres.NewQueueInMemoryFromString("inmem://localhost/error")
	if ha.replayRunCount != 0 || len(errQ.List) != 0 || ha.replaySuccess {
		t.Fatalf("TestError. len(errQ.List). expected: 0, actual: %d. ha.replayRunCount. expected: 0, actual: %d, ha.replaySuccess. expected: false, actual: %v", len(errQ.List), ha.replayRunCount, ha.replaySuccess)
	}

	h.Tick()
	if ha.replayRunCount != 1 || len(errQ.List) != 1 || ha.replaySuccess {
		t.Fatalf("TestError. len(errQ.List). expected: 1, actual: %d. ha.replayRunCount. expected: 1, actual: %d, ha.replaySuccess. expected: false, actual: %v", len(errQ.List), ha.replayRunCount, ha.replaySuccess)
	}
	checkenvelopeErrorCount(t, errQ.List[0], 1)

	payload, _ := errQ.Retrieve()
	h.LocalSendQ.Send(*payload)

	h.Tick()
	if ha.replayRunCount != 2 || len(errQ.List) != 1 || ha.replaySuccess {
		t.Fatalf("TestError. len(errQ.List). expected: 0, actual: %d. ha.replayRunCount. expected: 2, actual: %d, ha.replaySuccess. expected: false, actual: %v", len(errQ.List), ha.replayRunCount, ha.replaySuccess)
	}
	checkenvelopeErrorCount(t, errQ.List[0], 2)

	payload, _ = errQ.Retrieve()
	h.LocalSendQ.Send(*payload)

	h.Tick()
	if ha.replayRunCount != 3 || len(errQ.List) != 0 || !ha.replaySuccess {
		t.Fatalf("TestError. len(errQ.List). expected: 0, actual: %d. ha.replayRunCount. expected: 3, actual: %d, ha.replaySuccess. expected: true, actual: %v", len(errQ.List), ha.replayRunCount, ha.replaySuccess)
	}
}
