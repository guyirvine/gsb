package gsb

import (
	"context"
	"log"
	"net/url"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sqs"

	"gitlab.com/guyirvine/gsb/appres"
)

type mqSqs struct {
	url           *url.URL
	name          string
	queueURL      *string
	config        aws.Config
	svc           *sqs.Client
	receiptHandle *string
}

func newmqSqs(url *url.URL) *mqSqs {
	awsMq := new(mqSqs)
	awsMq.url = url
	if url.Path == "" {
		log.Fatalf("Must provide a queue name for newmqSqs. Environment Variable, MQ=%s\n", url.String())
	}
	awsMq.name = url.Path[1:]

	return awsMq
}

func (mq *mqSqs) getSvc() *sqs.Client {
	if mq.svc == nil {
		mq.svc = sqs.NewFromConfig(mq.config)
	}

	return mq.svc
}

func (mq *mqSqs) getQueueURL() error {
	// Create an SQS service client
	svc := mq.getSvc()

	result, err := svc.GetQueueUrl(context.TODO(),
		&sqs.GetQueueUrlInput{
			QueueName: &mq.name,
		})
	if err != nil {
		log.Printf("mq-sqs.getQueueURL.err. QueueName: %s", mq.name)
		mq.queueURL = nil
		return err
	}

	mq.queueURL = result.QueueUrl

	return nil
}

func (mq *mqSqs) setQueueTimeout() error {
	svc := mq.getSvc()

	return appres.AwsSetQueueTimeout(svc, mq.url, mq.queueURL)
}

func (mq *mqSqs) Init() error {
	mq.config = appres.NewAwsConfig()

	if err := mq.getQueueURL(); err != nil {
		return err
	}
	if err := mq.setQueueTimeout(); err != nil {
		return err
	}

	return nil
}

func (mq *mqSqs) Name() string {
	return mq.name
}

// Send wrapper for AWS SQS Queue
func (mq *mqSqs) Send(Message string) error {
	svc := mq.getSvc()

	_, err := svc.SendMessage(context.TODO(), &sqs.SendMessageInput{
		MessageBody: aws.String(Message),
		QueueUrl:    mq.queueURL,
	})
	if err != nil {
		return err
	}

	return nil
}

func (mq *mqSqs) Retrieve() (*string, error) {
	svc := mq.getSvc()

	msgResult, err := svc.ReceiveMessage(context.TODO(),
		&sqs.ReceiveMessageInput{
			QueueUrl:            mq.queueURL,
			MaxNumberOfMessages: 1,
			VisibilityTimeout:   25,
		})

	// snippet-end:[sqs.go.receive_messages.call]
	if err != nil {
		return nil, err
	}

	if len(msgResult.Messages) == 0 {
		return nil, nil
	}

	msg := msgResult.Messages[0]
	mq.receiptHandle = msg.ReceiptHandle

	return msg.Body, nil
}

// Begin Transaction
func (mq *mqSqs) Begin() error {
	return nil
}

func (mq *mqSqs) Commit() error {
	svc := mq.getSvc()

	_, err := svc.DeleteMessage(context.TODO(),
		&sqs.DeleteMessageInput{
			QueueUrl:      mq.queueURL,
			ReceiptHandle: mq.receiptHandle,
		})

	if err == nil {
		mq.receiptHandle = nil
	}

	return err
}

// Rollback Transaction
func (mq *mqSqs) Rollback() error {
	return nil
}
