module gitlab.com/guyirvine/gsb

go 1.16

require (
	github.com/aws/aws-sdk-go-v2 v1.9.2
	github.com/aws/aws-sdk-go-v2/config v1.8.3
	github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue v1.2.2
	github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression v1.2.5
	github.com/aws/aws-sdk-go-v2/service/dynamodb v1.5.2
	github.com/aws/aws-sdk-go-v2/service/s3 v1.16.1
	github.com/aws/aws-sdk-go-v2/service/sns v1.8.2
	github.com/aws/aws-sdk-go-v2/service/sqs v1.9.2
	github.com/beanstalkd/go-beanstalk v0.1.0
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/google/uuid v1.3.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/onsi/gomega v1.15.0 // indirect
	github.com/robfig/cron/v3 v3.0.1
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
)
