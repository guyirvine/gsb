package gsb

import (
	"strings"
	"log"

	"github.com/robfig/cron/v3"
)

type crons struct {
	messageSender messageSender
	c *cron.Cron
}

func newCrons(messageSender messageSender) *crons {
	cs := new(crons)
	cs.init(messageSender)
	return cs
}

func (cs *crons) init(messageSender messageSender) {
	cs.messageSender = messageSender
	strings := getGSBEnvVars("GSBCRON_")

	cs.loadCrons(strings)
}

func (cs *crons) loadCrons(gsbEnvVars []string) {
	cs.c = cron.New()
	for _, gsbEnvVarStr := range gsbEnvVars {
		parts := strings.SplitN(gsbEnvVarStr, "=", 2)

		_, err := cs.c.AddFunc(parts[1], func() {
			name := parts[0]
			cs.messageSender.Send(name, "")
		})
		if err != nil {
			log.Printf("Error loading a new Cron Job. Environment Variable: %s\n", gsbEnvVarStr)
			log.Fatal(err)
		}
	}
}

func (cs *crons) Start() {
	cs.c.Start()
}

func (cs *crons) Stop() {
	cs.c.Stop()
}
