package gsb

import (
	"encoding/json"
	"github.com/google/uuid"
	"time"
)

type envelopeError struct {
	Timestamp time.Time
	Error     string
}

type envelope struct {
	Identifier        string
	Name              string
	Msg               string
	OriginalQueueName string
	Errors            []envelopeError
}

func newEnvelope(QueueName, Name, Message string) (envelope, error) {
	var errors []envelopeError
	return envelope{uuid.New().String(), Name, Message, QueueName, errors}, nil
}

func (env *envelope) AddError(error string) {
	e := envelopeError{time.Now(), error}
	env.Errors = append(env.Errors, e)
}

func unmarshalEnvelope(payload []byte) (*envelope, error) {
	var env envelope
	err := json.Unmarshal(payload, &env)
	if err != nil {
		return nil, err
	}

	return &env, nil
}

func (env *envelope) Marshal() ([]byte, error) {
	payload, err := json.Marshal(env)
	if err != nil {
		return nil, err
	}

	return payload, nil
}
