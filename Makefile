#!make
include ~/.env

sonar:
	sonar-scanner \
		-Dsonar.projectKey=gsb \
		-Dsonar.sources=. \
		-Dsonar.host.url=${SONAR_GSB_HOST} \
		-Dsonar.login=${SONAR_GSB_LOGIN}

