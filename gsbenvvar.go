package gsb

import (
	"log"
	"net/url"
	"os"
	"strings"
)

type gsbEnvVar struct {
	name string
	url  *url.URL
}

func loadGsbEnvVars(prefix string) []gsbEnvVar {
	strings := getGSBEnvVars(prefix)
	return processEnvVars(strings)
}

func processEnvVar(gsbEnvVarStr string) gsbEnvVar {
	parts := strings.SplitN(gsbEnvVarStr, "=", 2)

	url, err := url.Parse(parts[1])
	if err != nil {
		log.Printf("Error parsing URL for app resource. Environment Variable: %s\n", gsbEnvVarStr)
		log.Fatal(err)
	}

	gsbEnvVar := gsbEnvVar{parts[0], url}

	return gsbEnvVar
}

func processEnvVars(gsbEnvVarStrs []string) []gsbEnvVar {
	var gsbEnvVars []gsbEnvVar
	for _, gsbEnvVarStr := range gsbEnvVarStrs {
		gsbEnvVar := processEnvVar(gsbEnvVarStr)
		gsbEnvVars = append(gsbEnvVars, gsbEnvVar)
	}

	return gsbEnvVars
}

func getGSBEnvVars(prefix string) []string {
	prefixLength := len(prefix)
	gsbEnvVars := []string{}
	for _, s := range os.Environ() {
		if len(s) > prefixLength && s[0:prefixLength] == prefix {
			gsbEnvVars = append(gsbEnvVars, s[prefixLength:])
		}
	}

	return gsbEnvVars
}
