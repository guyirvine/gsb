package gsb

import (
	"log"
)

type subscriber interface {
	Subscribe() error
}

type subscribers struct {
}

func (s *subscribers) Init() {
	gsbEnvVars := loadGsbEnvVars("GSBSUBS_")
	s.loadSubscriptions(gsbEnvVars)
}

func (s *subscribers) loadSubscriptions(gsbEnvVars []gsbEnvVar) {
	for _, gsbEnvVar := range gsbEnvVars {
		s.loadSubscriber(gsbEnvVar)
	}
}

func (s *subscribers) loadSubscriber(gsbEnvVar gsbEnvVar) {
	var subscriber subscriber
	url := gsbEnvVar.url
	switch url.Scheme {
	case "sns":
		subscriber = newSubscriberSns(url)
	default:
		log.Printf("Subscriber type not recognised: %s\n", url.Scheme)
		log.Fatalf("Environment Variable: GSBSUBS=%s\n", url.String())
	}

	subscriber.Subscribe()
	log.Printf("Added Subscription. Type: %s, Name: %s, Value: %s\n", url.Scheme, gsbEnvVar.name, url.String())
}
