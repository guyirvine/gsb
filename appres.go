package gsb

import (
	"gitlab.com/guyirvine/gsb/appres"

	"log"
)

type appResources struct {
	hash map[string]appres.AppRes
}

func newAppResources() *appResources {
	appRes := new(appResources)
	appRes.init()

	return appRes
}

func (ar *appResources) init() {
	ar.hash = make(map[string]appres.AppRes)

	gsbEnvVars := loadGsbEnvVars("GSB_")
	ar.loadAppResources(gsbEnvVars)
}

func (ar *appResources) loadAppResources(gsbEnvVars []gsbEnvVar) {
	for _, gsbEnvVar := range gsbEnvVars {
		ar.loadAppResource(gsbEnvVar)
		log.Printf("Added app resource. Type: %s, Name: %s, Value: %s\n", gsbEnvVar.url.Scheme, gsbEnvVar.name, gsbEnvVar.url.String())
	}
}

func (ar *appResources) loadAppResource(gsbEnvVar gsbEnvVar) {
	switch gsbEnvVar.url.Scheme {
	case "dynamodb":
		ar.hash[gsbEnvVar.name] = appres.NewDynamoDb(gsbEnvVar.url)
	case "inmemdynamodb":
		ar.hash[gsbEnvVar.name] = appres.NewMqInMemoryDynamoDb(gsbEnvVar.url)
	case "s3":
		ar.hash[gsbEnvVar.name] = appres.NewS3(gsbEnvVar.url)
	case "fs":
		ar.hash[gsbEnvVar.name] = appres.NewFS(gsbEnvVar.url)
	case "dir":
		ar.hash[gsbEnvVar.name] = appres.NewDir(gsbEnvVar.url)
	case "bucket":
		ar.hash[gsbEnvVar.name] = appres.NewBucket(gsbEnvVar.url)
	case "redis":
		ar.hash[gsbEnvVar.name] = appres.NewRedis(gsbEnvVar.url)
	case "inmemkeyval":
		ar.hash[gsbEnvVar.name] = appres.NewInMemKeyVal(gsbEnvVar.url)
	case "beanstalk":
		ar.hash[gsbEnvVar.name] = appres.NewQueueBeanstalk(gsbEnvVar.url)
	case "sqs":
		ar.hash[gsbEnvVar.name] = appres.NewQueueSqs(gsbEnvVar.url)
	case "sns":
		ar.hash[gsbEnvVar.name] = appres.NewPublisherSns(gsbEnvVar.url)
	case "inmemq":
		ar.hash[gsbEnvVar.name] = appres.NewQueueInMemory(gsbEnvVar.url)
	case "inmemp":
		ar.hash[gsbEnvVar.name] = appres.NewPublisherInMemory(gsbEnvVar.url)
	default:
		log.Fatalf("AppRes type not recognised: %v\n", gsbEnvVar.url.Scheme)
	}

}
