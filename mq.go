package gsb

import (
	"log"
	"net/url"
	"os"
)

type mq interface {
	Init() error
	Name() string
	Send(string) error
	Retrieve() (*string, error)
	Commit() error
}

func (h *Host) initMq() {
	urlString := os.Getenv("MQ")
	url, err := url.Parse(urlString)
	if err != nil {
		log.Printf("Got an error parsing mq url: %s\n", urlString)
		log.Fatal(err)
	}

	switch url.Scheme {
	case "beanstalk":
		h.Mq = newMqBeanstalk(url)
	case "sqs":
		h.Mq = newmqSqs(url)
	case "inmem":
		h.Mq = NewMqInMemory(url)
	default:
		log.Printf("Mq type not recognised: %s\n", url.Scheme)
		log.Fatalf("Environment Variable: MQ=%s\n", os.Getenv("MQ"))
	}

	err = h.Mq.Init()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Mq connected to: %s\n", url.String())

	h.MqURL = url

	h.LocalSendQ = h.Mq //appres.NewQueue(urlString)
}

func (h *Host) mqRetrieve() *string {
	payload, err := h.Mq.Retrieve()
	if err != nil {
		log.Printf("Fatal. Could not get message from MQ. %s\n", os.Getenv("MQ"))
		log.Fatal(err)
	}

	return payload
}

func (h *Host) mqCommit() error {
	return h.Mq.Commit()
}
