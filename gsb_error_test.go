package gsb

import (
	"errors"
	"os"
	"testing"

	"gitlab.com/guyirvine/gsb/appres"
)

type ErrorTestingHandler struct {
}

func (h *ErrorTestingHandler) Name() string {
	return "ErrorTestingHandler"
}

func (h *ErrorTestingHandler) Handle(_ string) error {
	return errors.New("Generated Error")
}

func setupForError() *Host {
	ResetMqInMemory()
	appres.ResetQueueInMemory()

	os.Setenv("NAME", "TESTING")
	os.Setenv("MQ", "inmem://localhost/test")
	os.Setenv("RETRIES", "0")

	h := NewHost()
	h.AddHandler(new(ErrorTestingHandler))

	h.Send("ErrorTestingHandler", "Test Message")

	return h
}

func TestError(t *testing.T) {
	h := setupForError()

	errQ := appres.NewQueueInMemoryFromString("inmem://localhost/error")
	if len(errQ.List) != 0 {
		t.Fatalf("TestError. len(errQ.List). expected: 0, actual: %d", len(errQ.List))
	}

	h.Tick()
	if len(errQ.List) != 1 {
		t.Fatalf("TestError. len(errQ.List). expected: 1, actual: %d", len(errQ.List))
	}
}
