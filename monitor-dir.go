package gsb

import (
	"io/ioutil"
	"log"
	"net/url"
	"os"
)

type monitorDir struct {
	url      *url.URL
	path     string
	fileName string
}

func newMonitorDir(url *url.URL) *monitorDir {
	dir := new(monitorDir)
	dir.url = url
	dir.path = url.Path

	return dir
}

func (dir *monitorDir) init() error {
	_, err := os.Stat(dir.path)
	if err != nil {
		if os.IsNotExist(err) {
			log.Printf("Requested Directory does not exist: %s\n", dir.path)
			log.Fatal(err)
		}
		log.Fatal(err)
	}

	return nil
}

func (dir *monitorDir) next() (*MonitorData, error) {
	files, err := ioutil.ReadDir(dir.path)
	if err != nil {
		return nil, err
	}

	if len(files) == 0 {
		return nil, nil
	}

	dir.fileName = files[0].Name()

	payload, err := ioutil.ReadFile(dir.path + "/" + dir.fileName)

	m := MonitorData{dir.fileName, string(payload)}

	return &m, nil
}

func (dir *monitorDir) commit() error {
	return os.Remove(dir.path + "/" + dir.fileName)
}
