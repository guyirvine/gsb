package gsb

import (
  "runtime"
	"fmt"
	"path/filepath"
)

// Errorf formats an error for GSB
func Errorf(error error) error {
  if (error == nil) {
    return nil
  }

		_, file, line, _ := runtime.Caller(1)
	location := fmt.Sprintf("%s:%d", filepath.Base(file), line)

  formattedError := fmt.Errorf("%s\n%s", error.Error(), location)

  return formattedError
}
