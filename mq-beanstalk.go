package gsb

import (
	"net/url"
	"time"

	"github.com/beanstalkd/go-beanstalk"

	"gitlab.com/guyirvine/gsb/appres"
)

type mqBeanstalk struct {
	url     *url.URL
	name    string
	jobID   uint64
	conn    *beanstalk.Conn
	tube    *beanstalk.Tube
	tubeSet *beanstalk.TubeSet
	timeout time.Duration
}

func newMqBeanstalk(url *url.URL) *mqBeanstalk {
	mq := new(mqBeanstalk)
	mq.url = url
	mq.name = mq.url.Path[1:]

	return mq
}

func (mq *mqBeanstalk) Init() error {
	conn, err := beanstalk.Dial("tcp", mq.url.Host)
	if err != nil {
		return err
	}
	mq.conn = conn

	timeout := appres.GetTimeoutFromURL(mq.url)

	mq.timeout, err = time.ParseDuration(timeout + "ms")
	if err != nil {
		return err
	}

	mq.tube = beanstalk.NewTube(conn, mq.name)

	tubeSet := beanstalk.NewTubeSet(conn, mq.name)
	mq.tubeSet = tubeSet

	return err
}

func (mq *mqBeanstalk) Name() string {
	return mq.name
}

// Send wrapper for Beanstalk Queue
func (mq *mqBeanstalk) Send(payload string) error {
	_, err := mq.tube.Put([]byte(payload), 0, 0, 0)

	if err != nil {
		return err
	}

	return nil
}

func (mq *mqBeanstalk) Retrieve() (*string, error) {
	id, body, err := mq.tubeSet.Reserve(mq.timeout)
	if err != nil {
		if err.Error() == "reserve-with-timeout: timeout" {
			return nil, nil
		}

		return nil, err
	}
	mq.jobID = id

	strBody := string(body)

	return &strBody, nil
}

// Begin Transaction
func (mq *mqBeanstalk) Begin() error {
	return nil
}

// Rollback Transaction
func (mq *mqBeanstalk) Commit() error {
	err := mq.conn.Delete(mq.jobID)

	return err
}

// Rollback Transaction
func (mq *mqBeanstalk) Rollback() error {
	return nil
}
