package gsb

import (
	"errors"
	"fmt"
	"os"
	"testing"
)

type RetryTestingHandler struct {
	retryRunCount int
	retrySuccess  bool
}

func (ha *RetryTestingHandler) Init() {
	ha.retryRunCount = 0
}

func (ha *RetryTestingHandler) Name() string {
	return "RetryTestingHandler"
}

// This will pass after two retries, meaning it will run three times
func (ha *RetryTestingHandler) Handle(_ string) error {
	ha.retryRunCount = ha.retryRunCount + 1
	if ha.retryRunCount < 4 {
		msg := fmt.Sprintf("Generated Error. %d", ha.retryRunCount)
		return errors.New(msg)
	}

	ha.retrySuccess = true

	return nil
}

func setupForRetry(retries string) *RetryTestingHandler {
	ResetMqInMemory()

	os.Setenv("NAME", "TESTING")
	os.Setenv("MQ", "inmem://localhost/test")
	os.Setenv("RETRIES", retries)

	h := NewHost()
	ha := new(RetryTestingHandler)
	h.AddHandler(ha)

	h.Send("RetryTesting", "Test Message")

	ha.retryRunCount = 0
	ha.retrySuccess = false

	h.Tick()

	return ha
}

func TestRetries3(t *testing.T) {
	ha := setupForRetry("3")
	if ha.retryRunCount != 4 || !ha.retrySuccess {
		t.Fatalf("TestRetries. retryRunCount. expected: 4, actual: %d, retrySuccess. expected: true, actual: %v", ha.retryRunCount, ha.retrySuccess)
	}
}

func TestRetries2(t *testing.T) {
	ha := setupForRetry("2")
	if ha.retryRunCount != 3 || ha.retrySuccess {
		t.Fatalf("TestRetries. retryRunCount. expected: 3, actual: %d, retrySuccess. expected: false, actual: %v", ha.retryRunCount, ha.retrySuccess)
	}
}
