package gsb

import (
	"os"
	"testing"

	"gitlab.com/guyirvine/gsb/appres"
)

type AppResAutoSetHandler struct {
	Dir     appres.KeyVal
	Counter int
	Wiggle  string
}

func (h *AppResAutoSetHandler) Name() string {
	return "AppResAutoSetHandler"
}

func (h *AppResAutoSetHandler) Handle(_ string) error {
	return nil
}

func setupForAppRes() *Host {
	ResetMqInMemory()

	os.Setenv("NAME", "TESTING")
	os.Setenv("MQ", "inmem://localhost/test")
	os.Setenv("GSB_Dir", "dir:///tmp")
	os.Setenv("RETRIES", "0")

	return NewHost()
}

func TestAppRes(t *testing.T) {
	h := setupForAppRes()

	ha := new(AppResAutoSetHandler)

	h.AddHandler(ha)
}
