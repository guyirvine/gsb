package gsb

import (
	"errors"
	"log"
	"reflect"
	"strings"

	"gitlab.com/guyirvine/gsb/appres"
)

// Bus is the Handler facing interface to GSB
type Bus interface {
	Send(string, string) error
	AppVerboseLog(string)
}

// Handler is to be implemented by user space projects
type Handler interface {
	Handle(payload string) error
}

type handlerWrapper struct {
	handler Handler
	appRes  []appres.AppRes
}

func (h *Host) getMessageNameFrom(HandlerName string) string {
	if strings.HasSuffix(HandlerName, "Handler") == false {
		log.Fatalf("Handler names must end with the word Handler, eg, " + HandlerName + "Handler")
	}
	idx := len(HandlerName) - len("Handler")
	MessageName := HandlerName[:idx]

	return MessageName
}

// AddHandler allows user space code to Add Handlers to GSB
func (h *Host) AddHandler(ha Handler) {
	elem := reflect.TypeOf(ha).Elem()

	handlerName := elem.Name()
	messageName := h.getMessageNameFrom(handlerName)
	if _, ok := h.Handler[handlerName]; ok == true {
		log.Fatalf("Handler already exists for message, %s\n", handlerName)
	}
	hw := new(handlerWrapper)
	hw.handler = ha
	h.Handler[messageName] = hw

	values := reflect.ValueOf(ha).Elem()
	for i := 0; i < elem.NumField(); i++ {
		field := elem.Field(i)

		if field.Name == "Bus" {
			values.Field(i).Set(reflect.ValueOf(h))
			log.Printf("Added Bus, to Handler, %s.\n", elem.Name())
		}
		if val, ok := h.appResources.hash[field.Name]; ok == true {
			values.Field(i).Set(reflect.ValueOf(val))
			hw.appRes = append(hw.appRes, val)
			log.Printf("Added AppResource, %s, to Handler, %s.\n", field.Name, elem.Name())
		}
	}

	// Call Init method, if it exists, after setting AppResources.
	st := reflect.ValueOf(ha)
	method := st.MethodByName("Init")
	if method.IsValid() == false {
		h.verboseLog("Handler, " + elem.Name() + ", had no Init function.")
	} else {
		ret := method.Call([]reflect.Value{})
		if len(ret) > 0 {
			err := ret[0]
			if err.IsZero() == false {
				log.Fatal(err)
			}
		}
	}

	log.Printf("Added Handler, %s, for Message, %s.\n", handlerName, messageName)
}

func (h *Host) processHandlers(env *envelope) error {
	h.verboseLog("Handler.processHandlers. env.Name: " + env.Name)
	if hw, ok := h.Handler[env.Name]; ok == true {
		h.verboseLog("Handler.processHandlers. Found Handler")

		for _, appRes := range hw.appRes {
			appRes.Begin()
		}

		if err := hw.handler.Handle(env.Msg); err != nil {
			for _, appRes := range hw.appRes {
				appRes.Rollback()
			}
			h.verboseLog("Handler.processHandlers. Err: " + err.Error())
			return err
		}
		for _, appRes := range hw.appRes {
			appRes.Commit()
		}
	} else {
		return errors.New("Handler not found for message, " + env.Name)
	}

	return nil
}
