package gsb

import (
	"encoding/json"
	"log"
	"gopkg.in/go-playground/validator.v9"
	"strconv"
	"os"
)

// MonitorData is supplied to a monitoring Handler
type MonitorData struct {
	Name    string `json:"Name" validate:"required"`
	Payload string `json:"Payload" validate:"required"`
}

type monitor interface {
	init() error
	next() (*MonitorData, error)
	commit() error
}

type monitors struct {
	monitors      map[string]monitor
	messageSender messageSender
	loopCount     int
}

func newMonitors(messageSender messageSender) *monitors {
	m := new(monitors)
	m.monitors = make(map[string]monitor)
	m.init(messageSender)
	return m
}

func (m *monitors) init(messageSender messageSender) {
	gsbEnvVars := loadGsbEnvVars("GSBMON_")
	m.loadMonitors(gsbEnvVars)
	m.messageSender = messageSender
	m.setLoopCount()
}

func (m *monitors) setLoopCount() {
	m.loopCount = 10
	count := os.Getenv("MONITOR_LOOPCOUNT")
	if count == "" {
		count = os.Getenv("LOOPCOUNT")
	}
	if count != "" {
		loopCount, err := strconv.Atoi(count)
		if err != nil {
			log.Printf("Environment Variable MONITOR_LOOPCOUNT must be a number. Actual: %s\n", count)
		}
		m.loopCount = loopCount
	}
	log.Printf("Monitor Loop Count: %v\n", m.loopCount)
}


func (m *monitors) loadMonitors(gsbEnvVars []gsbEnvVar) {
	for _, gsbEnvVar := range gsbEnvVars {
		m.loadMonitor(gsbEnvVar)
	}
}

func (m *monitors) loadMonitor(gsbEnvVar gsbEnvVar) {
	var monitor monitor
	url := gsbEnvVar.url
	switch url.Scheme {
	case "dir":
		monitor = newMonitorDir(url)
	case "sqs":
		monitor = newMonitorQueue(url)
	case "beanstalk":
		monitor = newMonitorQueue(url)
	case "inmem":
		monitor = newMonitorQueue(url)
	default:
		log.Printf("Monitor type not recognised: %s\n", url.Scheme)
		log.Fatalf("Environment Variable: GSBMON=%s\n", url.String())
	}

	// TODO allow querystring to add to loopcount per monitor
	monitor.init()
	m.monitors[gsbEnvVar.name] = monitor
	log.Printf("Added monitor. Type: %s, Name: %s, Value: %s\n", url.Scheme, gsbEnvVar.name, url.String())
}

func (m *monitors) process() {
	for name, monitor := range m.monitors {
		for i := 0; i < m.loopCount; i++ {
			monitorData, err := monitor.next()
			if monitorData == nil && err == nil {
				break
			}

			if err != nil {
				log.Fatal(err)
			}

			payload, err := json.Marshal(monitorData)
			if err != nil {
				log.Fatal(err)
			}

			m.messageSender.Send(name, string(payload))

			monitor.commit()
		}
	}
}

// ParseIncomingMonitorMessage is a user space helper function
func ParseIncomingMonitorMessage(payload string) (*MonitorData, error) {
	var md MonitorData
	if err := json.Unmarshal([]byte(payload), &md); err != nil {
		return nil, err
	}

	validator := validator.New()
	err := validator.Struct(md)
	err = Errorf(err)

	return &md, err
}
