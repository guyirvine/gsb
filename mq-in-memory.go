package gsb

import (
	"log"
	"net/url"
)

var (
	singleton map[string]*MqInMemory
)

// MqInMemory swap in queue for testing
type MqInMemory struct {
	url  *url.URL
	name string
	List []string
}

// ResetMqInMemory reset the singleton
func ResetMqInMemory() {
	singleton = nil
}

// NewMqInMemory returns the singleton, creating if necessary
func NewMqInMemory(url *url.URL) *MqInMemory {
	if singleton == nil {
		singleton = make(map[string]*MqInMemory)
	}
	name := url.Path[1:]

	if _, exists := singleton[name]; !exists {
		singleton[name] = new(MqInMemory)
		singleton[name].url = url
		singleton[name].name = url.Path[1:]
	}

	return singleton[name]
}

// NewMqInMemoryFromString adapter for NewMqInMemory
func NewMqInMemoryFromString(urlString string) *MqInMemory {
	urlObj, err := url.Parse(urlString)
	if err != nil {
		log.Printf("Error parsing URL for queue. urlString: %s\n", urlString)
		log.Fatal(err)
	}

	return NewMqInMemory(urlObj)
}

// Init wrapper for InMemory Queue
func (mq *MqInMemory) Init() error {
	return nil
}

// Name wrapper for InMemory Queue
func (mq *MqInMemory) Name() string {
	return mq.name
}

// Retrieve wrapper for InMemory Queue
func (mq *MqInMemory) Retrieve() (*string, error) {
	if len(mq.List) == 0 {
		return nil, nil
	}

	return &mq.List[0], nil
}

// Send wrapper for InMemory Queue
func (mq *MqInMemory) Send(payload string) error {
	mq.List = append(mq.List, payload)

	// TODO Need to link this to verbose logging
	log.Printf("MqInMemory.Send")

	return nil
}

// Move message to a different queue
func (mq *MqInMemory) Move(fromS string, toS string) {
	fromQ := singleton[fromS]

	payload := fromQ.List[0]
	fromQ.List = fromQ.List[1:]

	toQ := singleton[toS]
	toQ.List = append(toQ.List, payload)
}

// Begin Transaction
func (mq *MqInMemory) Begin() error {
	return nil
}

// Commit Transaction
func (mq *MqInMemory) Commit() error {
	mq.List = mq.List[1:]

	return nil
}

// Rollback Transaction
func (mq *MqInMemory) Rollback() error {
	return nil
}
