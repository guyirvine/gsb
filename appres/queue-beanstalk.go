package appres

import (
	"log"
	"net/url"
	"time"

	"github.com/beanstalkd/go-beanstalk"
)

// QueueBeanstalk is a wrapper for a Beanstalk tube
type QueueBeanstalk struct {
	url     *url.URL
	name    string
	conn    *beanstalk.Conn
	tube    *beanstalk.Tube
	tubeSet *beanstalk.TubeSet
	jobID   uint64
	timeout time.Duration
}

// NewQueueBeanstalk returns a configured Beanstalk App Resource
func NewQueueBeanstalk(url *url.URL) *QueueBeanstalk {
	q := new(QueueBeanstalk)
	q.url = url
	q.name = url.Path[1:]
	q.init()

	return q
}

func (q *QueueBeanstalk) init() {
	conn, err := beanstalk.Dial("tcp", q.url.Host)
	q.conn = conn
	q.tube = beanstalk.NewTube(conn, q.name)
	q.tubeSet = beanstalk.NewTubeSet(conn, q.name)

	timeout := GetTimeoutFromURL(q.url)

	q.timeout, err = time.ParseDuration(timeout + "s")
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("QueueBeanstalk. Connected to tube: %v\n", q.name)
}

// Name returns the name of the wrapped queue
func (q *QueueBeanstalk) Name() string {
	return q.name
}

// Send wrapper for Beanstalk Queue
func (q *QueueBeanstalk) Send(payload string) error {
	_, err := q.tube.Put([]byte(payload), 0, 0, 0)

	if err != nil {
		return err
	}

	return nil
}

// Retrieve wrapper for Beanstalk Queue
func (q *QueueBeanstalk) Retrieve() (*string, error) {
	id, body, err := q.tubeSet.Reserve(q.timeout)

	if err != nil {
		if err.Error() == "reserve-with-timeout: timeout" {
			return nil, nil
		}
		panic(err)
	}
	q.jobID = id

	strBody := string(body)

	return &strBody, nil
}

// Begin transaction
func (q *QueueBeanstalk) Begin() error {
	return nil
}

// Commit transaction
func (q *QueueBeanstalk) Commit() error {
	err := q.conn.Delete(q.jobID)

	return err
}

// Rollback transaction
func (q *QueueBeanstalk) Rollback() error {
	return nil
}
