package appres

import (
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"

	"bytes"
	"context"
	"io/ioutil"
	"log"
	"net/url"
)

// S3 is a wrapper for S3
type S3 struct {
	url    *url.URL
	config aws.Config
}

// NewS3 returns a configured S3 App Resource
func NewS3(url *url.URL) *S3 {
	s3 := new(S3)
	s3.url = url
	s3.init()

	return s3
}

func (s *S3) init() {
	s.config = NewAwsConfig()
}

// Put wrapper for S3
func (s *S3) Put(path string, name string, payload []byte) error {
	svc := s3.NewFromConfig(s.config)

	_, err := svc.PutObject(context.TODO(), &s3.PutObjectInput{
		Body:   bytes.NewReader(payload),
		Bucket: aws.String(path),
		Key:    aws.String(name),
	})
	if err != nil {
		log.Fatalf("Got error calling PutItem: %s", err)
	}

	return nil
}

// Get wrapper for S3
func (s *S3) Get(path string, name string) ([]byte, error) {
	svc := s3.NewFromConfig(s.config)

	result, err := svc.GetObject(context.TODO(), &s3.GetObjectInput{
		Bucket: aws.String(path),
		Key:    aws.String(name),
	})
	if err != nil {
		return nil, err
	}

	readBuf, _ := ioutil.ReadAll(result.Body)

	return readBuf, err
}

// Del wrapper for S3
func (s *S3) Del(path string, name string) error {
	svc := s3.NewFromConfig(s.config)

	_, err := svc.DeleteObject(context.TODO(), &s3.DeleteObjectInput{
		Bucket: aws.String(path),
		Key:    aws.String(name),
	})
	if err != nil {
		return err
	}

	return err
}

// Begin transaction
func (s *S3) Begin() error {
	return nil
}

// Commit transaction
func (s *S3) Commit() error {
	return nil
}

// Rollback transaction
func (s *S3) Rollback() error {
	return nil
}
