package appres

import (
	"github.com/aws/aws-sdk-go-v2/aws"
	awshttp "github.com/aws/aws-sdk-go-v2/aws/transport/http"
	"github.com/aws/aws-sdk-go-v2/service/s3"

	"net/http"

	"bytes"
	"context"
	"errors"
	"io/ioutil"
	"log"
	"net/url"
)

// Bucket wrapper for S3 Bucket
type Bucket struct {
	url    *url.URL
	name   string
	config aws.Config
}

// NewBucket returns a configured S3 Bucket App Resource
func NewBucket(url *url.URL) *Bucket {
	b := new(Bucket)
	b.url = url
	b.name = url.Path[1:]
	b.init()

	return b
}

func (b *Bucket) init() {
	b.config = NewAwsConfig()
}

func (b *Bucket) newFromConfig() (*s3.Client) {
	return s3.NewFromConfig(b.config, func(o *s3.Options) {
    o.UsePathStyle = true
  })
}

// Put wrapper for S3 Bucket
func (b *Bucket) Put(name string, item interface{}) error {
	svc := b.newFromConfig()

	_, err := svc.PutObject(context.TODO(), &s3.PutObjectInput{
		Body:   bytes.NewReader(item.([]byte)),
		Bucket: aws.String(b.name),
		Key:    aws.String(name),
	})
	if err != nil {
		log.Printf("Got error calling PutItem: %s", err)
		return err
	}

	return nil
}

// Get wrapper for S3 Bucket
func (b *Bucket) Get(name string) (interface{}, error) {
	svc := b.newFromConfig()

	result, err := svc.GetObject(context.TODO(), &s3.GetObjectInput{
		Bucket: aws.String(b.name),
		Key:    aws.String(name),
	})
	if err != nil {
		return nil, err
	}

	readBuf, _ := ioutil.ReadAll(result.Body)

	return readBuf, err
}

// Exists wrapper for S3 Bucket
func (b *Bucket) Exists(name string) (bool, error) {
	svc := b.newFromConfig()
	_, err := svc.HeadObject(context.TODO(), &s3.HeadObjectInput{
		Bucket: aws.String(b.name),
		Key:    aws.String(name),
	})
	if err != nil {
		var responseError *awshttp.ResponseError
		if errors.As(err, &responseError) && responseError.ResponseError.HTTPStatusCode() == http.StatusNotFound {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// Del wrapper for S3 Bucket
func (b *Bucket) Del(name string) error {
	svc := b.newFromConfig()
	_, err := svc.DeleteObject(context.TODO(), &s3.DeleteObjectInput{
		Bucket: aws.String(b.name),
		Key:    aws.String(name),
	})
	if err != nil {
		return err
	}

	return err
}

// Begin transaction
func (b *Bucket) Begin() error {
	return nil
}

// Commit transaction
func (b *Bucket) Commit() error {
	return nil
}

// Rollback transaction
func (b *Bucket) Rollback() error {
	return nil
}
