package appres

import (
	"context"
	"log"
	"net/url"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
)

// QueueSqs is a wrapper for a AWS SQS Queue
type QueueSqs struct {
	url           *url.URL
	name          string
	queueURL      *string
	config        aws.Config
	receiptHandle *string
}

// NewQueueSqs returns a configured Sqs App Resource
func NewQueueSqs(url *url.URL) *QueueSqs {
	awsSqs := new(QueueSqs)
	awsSqs.url = url
	if url.Path == "" {
		log.Fatalf("Must provide a queue name for newmqSqs. Environment Variable, MQ=%s\n", url.String())
	}
	awsSqs.name = url.Path[1:]

	awsSqs.init()

	return awsSqs
}

func (q *QueueSqs) getQueueURL(queueName string) (*sqs.GetQueueUrlOutput, error) {
	svc := sqs.NewFromConfig(q.config)

	log.Printf("appres-queue-aws.GetQueueUrl.1. Queue Name: %s\n", queueName)
	result, err := svc.GetQueueUrl(context.TODO(), &sqs.GetQueueUrlInput{
		QueueName: &queueName,
	})
	if err != nil {
		log.Printf("appres-queue-aws.GetQueueUrl. Could be that the queue does not exist. Queue Name: %s\n", queueName)
		log.Fatal(err)
		return nil, err
	}

	return result, nil
}

// Name returns the name of the wrapped queue
func (q *QueueSqs) Name() string {
	return q.name
}

// CreateQueue ensures the named queue exists
func (q *QueueSqs) CreateQueue() {
	svc := sqs.NewFromConfig(q.config)

	input := &sqs.CreateQueueInput{
		QueueName: &q.name,
		Attributes: map[string]string{
			"DelaySeconds":           "25",
			"MessageRetentionPeriod": "86400",
		},
	}

	_, err := svc.CreateQueue(context.TODO(), input)
	if err != nil {
		log.Printf("Got an error creating the queue: %s", q.name)
		log.Fatal(err)
		return
	}
}

func (q *QueueSqs) setQueueTimeout() error {
	svc := sqs.NewFromConfig(q.config)

	return AwsSetQueueTimeout(svc, q.url, q.queueURL)
}

func (q *QueueSqs) init() {
	q.config = NewAwsConfig()

	// Retrieve URL of queue
	result, err := q.getQueueURL(q.name)
	if err != nil {
		log.Printf("appres-queue-aws.GetQueueUrl. Could be that the queue does not exist. Queue Name: %s\n", q.name)
		log.Fatal(err)
	}

	q.queueURL = result.QueueUrl

//	if err := q.setQueueTimeout(); err != nil {
//		log.Printf("Error occured: appres-queue-aws.setQueueTimeout. Queue Name: %s\n", q.name)
//		log.Fatal(err)
//	}
}

// Send wrapper for AWS SQS Queue
func (q *QueueSqs) Send(Message string) error {
	svc := sqs.NewFromConfig(q.config)

	_, err := svc.SendMessage(context.TODO(), &sqs.SendMessageInput{
		MessageBody: aws.String(Message),
		QueueUrl:    q.queueURL,
	})
	if err != nil {
		return err
	}

	return nil
}

// Retrieve wrapper for AWS SQS Queue
func (q *QueueSqs) Retrieve() (*string, error) {
	svc := sqs.NewFromConfig(q.config)

	msgResult, err := svc.ReceiveMessage(context.TODO(), &sqs.ReceiveMessageInput{
		QueueUrl:            q.queueURL,
		MaxNumberOfMessages: 1,
		VisibilityTimeout:   25,
	})

	// snippet-end:[sqs.go.receive_messages.call]
	if err != nil {
		return nil, err
	}

	if len(msgResult.Messages) == 0 {
		return nil, nil
	}

	msg := msgResult.Messages[0]
	q.receiptHandle = msg.ReceiptHandle
	return msg.Body, nil
}

// Begin transaction
func (q *QueueSqs) Begin() error {
	return nil
}

// Commit transaction
func (q *QueueSqs) Commit() error {
	svc := sqs.NewFromConfig(q.config)

	_, err := svc.DeleteMessage(context.TODO(), &sqs.DeleteMessageInput{
		QueueUrl:      q.queueURL,
		ReceiptHandle: q.receiptHandle,
	})

	return err
}

// Rollback transaction
func (q *QueueSqs) Rollback() error {
	return nil
}
