package appres

import (
	"errors"
	"io/ioutil"
	"log"
	"net/url"
	"os"
)

// Dir is a wrapper for a directory
type Dir struct {
	url  *url.URL
	path string
}

// NewDir returns a configured Directory App Resource
func NewDir(url *url.URL) *Dir {
	dir := new(Dir)
	dir.url = url
	dir.path = url.Path
	dir.init()

	return dir
}

func (dir *Dir) init() {
	_, err := os.Stat(dir.path)
	if err != nil {
		if os.IsNotExist(err) {
			log.Printf("Requested Directory does not exist: %s\n", dir.path)
			log.Fatal(err)
		}
		log.Fatal(err)
	}
}

// Put wrapper for Directory
func (dir *Dir) Put(name string, item interface{}) error {
	return ioutil.WriteFile(dir.path+"/"+name, item.([]byte), 0644)
}

// Get wrapper for S3 Bucket
func (dir *Dir) Get(name string) (interface{}, error) {
	exists, err := dir.Exists(name)
	if err != nil {
		return nil, err
	}

	if exists {
		payload, err := ioutil.ReadFile(dir.path + "/" + name)
		return payload, err
	}

	return nil, errors.New("Path does not exist, " + dir.path + "/" + name)
}

// Exists wrapper for S3 Bucket
func (dir *Dir) Exists(name string) (bool, error) {
	_, err := os.Stat(dir.path + "/" + name)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// Del wrapper for S3 Bucket
func (dir *Dir) Del(name string) error {
	return os.Remove(dir.path + "/" + name)
}

// Begin transaction
func (dir *Dir) Begin() error {
	return nil
}

// Commit transaction
func (dir *Dir) Commit() error {
	return nil
}

// Rollback transaction
func (dir *Dir) Rollback() error {
	return nil
}
