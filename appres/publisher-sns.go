package appres

import (
	"context"
	"log"
	"net/url"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sns"
)

// PublisherSns is a wrapper for a AWS SNS Topic
type PublisherSns struct {
	url           *url.URL
	name          string
	config        aws.Config
	topicARN			string
}

// NewPublisherSns returns a configured Sqs App Resource
func NewPublisherSns(url *url.URL) *PublisherSns {
	sns := new(PublisherSns)
	sns.url = url
	if url.Path == "" {
		log.Fatalf("Must provide a queue name for NewPublisherSns. Environment Variable, %s\n", url.String())
	}
	sns.name = url.Path[1:]

	sns.init()

	return sns
}

// Name returns the name of the wrapped queue
func (q *PublisherSns) Name() string {
	return q.name
}

func (q *PublisherSns) setTopicARN() {
	if q.topicARN = q.url.Query().Get("topicARN"); q.topicARN == "" {
		log.Fatalf("topicARN is missing for SNS topic, %s. url: %s", q.url.Path, q.url.String())
	}
}

func (q *PublisherSns) init() {
	q.config = NewAwsConfig()

	q.setTopicARN()
}

// Publish wrapper for AWS SNS Topic
func (q *PublisherSns) Publish(Message string) error {
	svc := sns.NewFromConfig(q.config)

	_, err := svc.Publish(context.TODO(), &sns.PublishInput{
		Message:  aws.String(Message),
		TopicArn: &q.topicARN,
	})

	if err != nil {
		return err
	}

	return nil
}

// Begin transaction
func (q *PublisherSns) Begin() error {
	return nil
}

// Commit transaction
func (q *PublisherSns) Commit() error {
	return nil
}

// Rollback transaction
func (q *PublisherSns) Rollback() error {
	return nil
}
