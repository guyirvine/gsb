package appres

import (
	"net/url"
	"log"
)

// PublisherInMemory is a wrapper for a AWS SNS Topic
type PublisherInMemory struct {
	url           *url.URL
	name          string
	List					[]string
}

// NewPublisherInMemory returns an InMemory Publisher for testing purposes
func NewPublisherInMemory(url *url.URL) *PublisherInMemory {
	p := new(PublisherInMemory)
	p.url = url
	if url.Path == "" {
		log.Fatalf("Must provide a queue name for NewPublisherInMemory. Environment Variable, %s\n", url.String())
	}
	p.name = url.Path[1:]

	p.init()

	return p
}

// Name returns the name of the wrapped queue
func (p *PublisherInMemory) Name() string {
	return p.name
}

func (p *PublisherInMemory) init() {
	p.List = []string{}
}

// Publish wrapper for AWS SNS Topic
func (p *PublisherInMemory) Publish(Message string) error {
	p.List = append(p.List, Message)
	return nil
}

// Begin transaction
func (p *PublisherInMemory) Begin() error {
	return nil
}

// Commit transaction
func (p *PublisherInMemory) Commit() error {
	return nil
}

// Rollback transaction
func (p *PublisherInMemory) Rollback() error {
	return nil
}
