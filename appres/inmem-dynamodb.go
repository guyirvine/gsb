package appres

import (
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"

	"log"
	"fmt"
	"net/url"
)

var (
	singletonInMemoryDynamoDb map[string]*MqInMemoryDynamoDb
)

// MqInMemoryDynamoDb is swap in unit testing involving calls to DynamoDb
type MqInMemoryDynamoDb struct {
	url        *url.URL
	name       string
	TableNames []string
	List       []interface{}
}

// ResetMqInMemoryDynamoDb resets the singleton
func ResetMqInMemoryDynamoDb() {
	singletonInMemoryDynamoDb = nil
}

// NewMqInMemoryDynamoDb returns the singleton, creating if necessary
func NewMqInMemoryDynamoDb(url *url.URL) *MqInMemoryDynamoDb {
	if singletonInMemoryDynamoDb == nil {
		singletonInMemoryDynamoDb = make(map[string]*MqInMemoryDynamoDb)
	}
	if len(url.Path) < 2 {
		log.Fatalf("MqInMemoryDynamoDb. Name missing in path section of url, %s\n", url.String())
	}
	name := url.Path[1:]

	if _, exists := singletonInMemoryDynamoDb[name]; !exists {
		singletonInMemoryDynamoDb[name] = new(MqInMemoryDynamoDb)
		singletonInMemoryDynamoDb[name].url = url
		singletonInMemoryDynamoDb[name].name = url.Path[1:]
	}

	return singletonInMemoryDynamoDb[name]
}

// NewMqInMemoryDynamoDbFromString is an adapter for NewMqInMemoryDynamoDb
func NewMqInMemoryDynamoDbFromString(urlString string) *MqInMemoryDynamoDb {
	urlObj, err := url.Parse(urlString)
	if err != nil {
		log.Printf("Error parsing URL for queue. urlString: %s\n", urlString)
		log.Fatal(err)
	}

	return NewMqInMemoryDynamoDb(urlObj)
}

// CreateTable is a wrapper for creating mock S3 Tables
func (ddb *MqInMemoryDynamoDb) CreateTable(input *dynamodb.CreateTableInput) error {
	ddb.TableNames = append(ddb.TableNames, *input.TableName)
	return nil
}

// ListTables is a wrapper for listing mock S3 Tables
func (ddb *MqInMemoryDynamoDb) ListTables() ([]string, error) {
	return ddb.TableNames, nil
}

// Put is a wrapper for putting data into mock S3 Tables
func (ddb *MqInMemoryDynamoDb) Put(tableName string, item interface{}) error {
	verboseLog(fmt.Sprintf("MqInMemoryDynamoDb.Put. 1. tableName: %s, item: %v\n", tableName, item))
	ddb.List = append(ddb.List, item)

	return nil
}

// Get is a wrapper for getting data from mock S3 Tables
func (ddb *MqInMemoryDynamoDb) Get(tableName string, key map[string]string) (map[string]types.AttributeValue, error) {

	if len(ddb.List) == 0 {
		e := new(DynamoDbItemNotFound)
		e.TableName = tableName
		e.Key = key
		return nil, e
	}

	av, err := attributevalue.MarshalMap(ddb.List[0])
	if err != nil {
		return nil, err
	}

	return av, nil
}

// Begin transaction
func (ddb *MqInMemoryDynamoDb) Begin() error {
	return nil
}

// Commit transaction
func (ddb *MqInMemoryDynamoDb) Commit() error {
	return nil
}

// Rollback transaction
func (ddb *MqInMemoryDynamoDb) Rollback() error {
	return nil
}
