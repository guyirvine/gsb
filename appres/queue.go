package appres

import (
	"log"
	"net/url"
)

type appResQueue interface {
	Name() string
	Send(payload string) error
	Retrieve() (*string, error)
	commit() error
}

// Queue is the generic interface to a queue
type Queue interface {
	Name() string
	Send(payload string) error
	Retrieve() (*string, error)
	Commit() error
}

// NewQueue allows queues to be created and returned, based on the url string provided.
func NewQueue(urlString string) Queue {
	urlObj, err := url.Parse(urlString)
	if err != nil {
		log.Printf("Error parsing URL for queue. Environment Variable: %s\n", urlString)
		log.Fatal(err)
	}

	var queue Queue
	switch urlObj.Scheme {
	case "beanstalk":
		queue = NewQueueBeanstalk(urlObj)
	case "sqs":
		queue = NewQueueSqs(urlObj)
	case "inmem":
		queue = NewQueueInMemory(urlObj)
	default:
		log.Fatalf("Queue type not recognised. Environment Variable: %s\n", urlString)
	}

	return queue
}

// GetTimeoutFromURL retrieve value from querystring parameter timeout
func GetTimeoutFromURL(qURL *url.URL) string {
	timeout := qURL.Query().Get("timeout")

	if timeout == "" {
		return "3"
	}

	return timeout
}
