package appres

import (
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"

	"context"
	"fmt"
	"log"
	"net/url"
)

// DynamoDb is a wrapper for AWS DynamoDb
type DynamoDb struct {
	url    *url.URL
	config aws.Config
}

// DynamoDbItemNotFound is a signal
type DynamoDbItemNotFound struct {
	TableName string
	Key interface{}
}

func (e *DynamoDbItemNotFound) Error() string {
    return fmt.Sprintf("Could not find item from table, %s, key: %v", e.TableName, e.Key)
}

// NewDynamoDb return a configured DynamoDb Resource
func NewDynamoDb(url *url.URL) *DynamoDb {
	dynamoDb := new(DynamoDb)
	dynamoDb.url = url

	dynamoDb.init()

	return dynamoDb
}

func (ddb *DynamoDb) init() {
	ddb.config = NewAwsConfig()
}

// CreateTable is a wrapper for creating S3 Tables
func (ddb *DynamoDb) CreateTable(input *dynamodb.CreateTableInput) error {
	svc := dynamodb.NewFromConfig(ddb.config)
	_, err := svc.CreateTable(context.TODO(), input)

	return err
}

// ListTables is a wrapper for listing S3 Tables
func (ddb *DynamoDb) ListTables() ([]string, error) {
	svc := dynamodb.NewFromConfig(ddb.config)
	input := &dynamodb.ListTablesInput{}
	output, err := svc.ListTables(context.TODO(), input)

	if err != nil {
		return nil, err
	}

	return output.TableNames, nil
}

// Put is a wrapper for putting data in a DynamoDb table
func (ddb *DynamoDb) Put(tableName string, item interface{}) error {
	svc := dynamodb.NewFromConfig(ddb.config)

	av, err := attributevalue.MarshalMap(item)
	if err != nil {
		log.Fatalf("Got error marshalling item for Put: %v", err)
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}

	if _, err = svc.PutItem(context.TODO(), input); err != nil {
		log.Printf("Got error calling PutItem: %s", err)
		return err
	}

	return nil
}

// PutConditional is a wrapper for putting data in a DynamoDb table
func (ddb *DynamoDb) PutConditional(tableName string, item interface{}, condition *string, conditionAttributes map[string]interface{}) error {
	svc := dynamodb.NewFromConfig(ddb.config)

	av, err := attributevalue.MarshalMap(item)
	if err != nil {
		log.Fatalf("Got error marshalling item for Put: %v", err)
	}

	ca, err := attributevalue.MarshalMap(conditionAttributes)
	if err != nil {
		log.Fatalf("Got error marshalling conditional attributes for Put: %v", err)
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
		ConditionExpression: condition,
		ExpressionAttributeValues: ca,
	}

	if _, err = svc.PutItem(context.TODO(), input); err != nil {
		log.Printf("Got error calling PutItem: %s", err)
		return err
	}

	return nil
}

// Get is a wrapper for getting data from a DynamoDb table
func (ddb *DynamoDb) Get(tableName string, key map[string]string) (map[string]types.AttributeValue, error) {
	svc := dynamodb.NewFromConfig(ddb.config)

	av, err := attributevalue.MarshalMap(key)
	result, err := svc.GetItem(context.TODO(), &dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key:       av,
	})
	if err != nil {
		log.Printf("Error calling GetItem: %s", err)
		return nil, err
	}

	if result.Item == nil {
		e := new(DynamoDbItemNotFound)
		e.TableName = tableName
		e.Key = key
		return nil, e
	}

	return result.Item, nil
}

// Del is a wrapper for removing an item from a DynamoDb table
func (ddb *DynamoDb) Del(tableName string, key map[string]string) (error) {
	svc := dynamodb.NewFromConfig(ddb.config)

	av, err := attributevalue.MarshalMap(key)
	_, err = svc.DeleteItem(context.TODO(), &dynamodb.DeleteItemInput{
		TableName: aws.String(tableName),
		Key:       av,
	})
	if err != nil {
		log.Printf("Error calling DeleteItem: %s", err)
		return err
	}

	return nil
}

// Query is a wrapper for getting data from a DynamoDb table
func (ddb *DynamoDb) Query(queryInput *dynamodb.QueryInput) ([]map[string]types.AttributeValue, error) {
	svc := dynamodb.NewFromConfig(ddb.config)

	result, err := svc.Query(context.TODO(), queryInput)
	if err != nil {
		log.Printf("Error calling Query: %s", err)
		return nil, err
	}

	return result.Items, nil
}

// Scan is a wrapper for getting data from a DynamoDb table
func (ddb *DynamoDb) Scan(tableName string, filter expression.ConditionBuilder) ([]map[string]types.AttributeValue, error) {
	svc := dynamodb.NewFromConfig(ddb.config)

	expr, err := expression.NewBuilder().WithFilter(filter).Build()
	if err != nil {
		return nil, err
	}

	input := &dynamodb.ScanInput{
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	}

	// result, err := svc.ScanWithContext(context.TODO(), scanInput)
	result, err := svc.Scan(context.TODO(), input)
	if err != nil {
		log.Printf("Error calling Scan: %s", err)
		return nil, err
	}

	return result.Items, nil
}

// Begin transaction
func (ddb *DynamoDb) Begin() error {
	return nil
}

// Commit transaction
func (ddb *DynamoDb) Commit() error {
	return nil
}

// Rollback transaction
func (ddb *DynamoDb) Rollback() error {
	return nil
}
