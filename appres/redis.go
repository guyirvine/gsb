package appres

import (
	"github.com/go-redis/redis"

	"errors"
	"net/url"
)

// Redis is a wrapper for a Redis
type Redis struct {
	url     *url.URL
	session *redis.Client
}

// NewRedis returns a configured Redis App Resource
func NewRedis(url *url.URL) *Redis {
	redis := new(Redis)
	redis.url = url
	redis.init()

	return redis
}

func (r *Redis) init() {
	r.session = redis.NewClient(&redis.Options{
		Addr:     r.url.Host,
		Password: "",
		DB:       0,
	})
}

// Put wrapper for Redis
func (r *Redis) Put(key string, item interface{}) error {
	return r.session.Set(key, item, 0).Err()
}

// Get wrapper for Redis
func (r *Redis) Get(key string) (interface{}, error) {
	return r.session.Get(key).Result()
}

// Exists wrapper for Redis
func (r *Redis) Exists(name string) (bool, error) {
	return false, errors.New("function not implemented")
}

// Del wrapper for Redis
func (r *Redis) Del(key string) error {
	_, err := r.session.Del(key).Result()
	return err
}

// Begin transaction
func (r *Redis) Begin() error {
	return nil
}

// Commit transaction
func (r *Redis) Commit() error {
	return nil
}

// Rollback transaction
func (r *Redis) Rollback() error {
	return nil
}
