package appres

import (
	"io/ioutil"
	"net/url"
	"os"
)

// FS is a wrapper for a file
type FS struct {
	url *url.URL
}

// NewFS returns a configured File System App Resource
func NewFS(url *url.URL) *FS {
	fs := new(FS)
	fs.url = url
	fs.init()

	return fs
}

func (fs *FS) init() {
  // With both path and filename passed in, no Init required for FS, but appres.AppRes interface requires it
}

// Put wrapper for FileSystem
func (fs *FS) Put(path string, name string, payload []byte) error {
	err := ioutil.WriteFile(path+"/"+name, payload, 0644)

	return err
}

// Get wrapper for FileSystem
func (fs *FS) Get(path string, name string) ([]byte, error) {
	payload, err := ioutil.ReadFile(path + "/" + name)

	return payload, err
}

// Del wrapper for FileSystem
func (fs *FS) Del(path string, name string) error {
	err := os.Remove(path + "/" + name)
	return err
}

// Begin transaction
func (fs *FS) Begin() error {
	return nil
}

// Commit transaction
func (fs *FS) Commit() error {
	return nil
}

// Rollback transaction
func (fs *FS) Rollback() error {
	return nil
}
