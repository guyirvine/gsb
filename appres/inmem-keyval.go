package appres

import (
	"net/url"
)

// InMemKeyVal is swap in unit testing involving calls to KeyVal stores
type InMemKeyVal struct {
	url    *url.URL
	values map[string]interface{}
}

// NewInMemKeyVal returns a configured In Memory Key Value App Resource
func NewInMemKeyVal(url *url.URL) *InMemKeyVal {
	r := new(InMemKeyVal)
	r.url = url
	r.init()

	return r
}

func (r *InMemKeyVal) init() {
	r.values = make(map[string]interface{})
}

// Put wrapper for InMemory Key Value Store
func (r *InMemKeyVal) Put(key string, item interface{}) error {
	r.values[key] = item

	return nil
}

// Get wrapper for InMemory Key Value Store
func (r *InMemKeyVal) Get(key string) (interface{}, error) {
	return r.values[key], nil
}

// Exists wrapper for InMemory Key Value Store
func (r *InMemKeyVal) Exists(key string) (bool, error) {
	_, ok := r.values[key]

	return ok, nil
}

// Del wrapper for InMemory Key Value Store
func (r *InMemKeyVal) Del(key string) error {
	delete(r.values, key)

	return nil
}

// Begin transaction
func (r *InMemKeyVal) Begin() error {
	return nil
}

// Commit transaction
func (r *InMemKeyVal) Commit() error {
	return nil
}

// Rollback transaction
func (r *InMemKeyVal) Rollback() error {
	return nil
}
