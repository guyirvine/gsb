package appres

import (
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/sqs"

	"context"
	"log"
	"fmt"
	"net/url"

	"os"
)

// NewAwsConfig configures aws connection
func NewAwsConfig() aws.Config {
	awsRegion := os.Getenv("AWS_REGION")

	customResolver := aws.EndpointResolverFunc(func(service, region string) (aws.Endpoint, error) {
		awsEndpoint := os.Getenv("AWS_ENDPOINT_" + service)
		if awsEndpoint == "" {
			awsEndpoint = os.Getenv("AWS_ENDPOINT")
		}
		if awsEndpoint != "" {
			verboseLog(fmt.Sprintf("Using service: %s, awsEndpoint: %s, awsRegion: %s\n", service, awsEndpoint, awsRegion))
		}

		if awsEndpoint != "" {
			return aws.Endpoint{
				PartitionID:   "aws",
				URL:           awsEndpoint,
				SigningRegion: awsRegion,
			}, nil
		}

		// returning EndpointNotFoundError will allow the service to fallback to it's default resolution
		return aws.Endpoint{}, &aws.EndpointNotFoundError{}
	})

	awsCfg, err := config.LoadDefaultConfig(context.TODO(),
		config.WithRegion(awsRegion),
		config.WithEndpointResolver(customResolver),
	)
	if err != nil {
		log.Fatalf("Cannot load AWS config: %v", err)
	}

	return awsCfg
}
/*
// NewAwsConfigForSQS configures aws connection
func NewAwsConfigForSQS() aws.Config {
	awsEndpoint := os.Getenv("AWS_ENDPOINT_SQS")

	return NewAwsConfig(awsEndpoint)
}

// NewAwsConfigForS3 configures aws connection
func NewAwsConfigForS3() aws.Config {
	awsEndpoint := os.Getenv("AWS_ENDPOINT_S3")

	return NewAwsConfig(awsEndpoint)
}

// NewAwsConfigForSNS configures aws connection
func NewAwsConfigForSNS() aws.Config {
	awsEndpoint := os.Getenv("AWS_ENDPOINT_SNS")

	return NewAwsConfig(awsEndpoint)
}

// NewAwsConfigForDDB configures aws connection
func NewAwsConfigForDDB() aws.Config {
	awsEndpoint := os.Getenv("AWS_ENDPOINT_DDB")

	return NewAwsConfig(awsEndpoint)
}
*/
// AwsSetQueueTimeout sets timeout for sqs queue
func AwsSetQueueTimeout(svc *sqs.Client, envURL *url.URL, queueURL *string) error {
	timeout := GetTimeoutFromURL(envURL)

	_, err := svc.SetQueueAttributes(context.TODO(), &sqs.SetQueueAttributesInput{
		QueueUrl: queueURL,
		Attributes: map[string]string{
			"ReceiveMessageWaitTimeSeconds": timeout,
		},
	})

	return err
}
