package appres

import (
	"log"
	"os"

	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
)

// KeyVal interface for allowing swap outs for testing
type KeyVal interface {
	Put(key string, item interface{}) error
	Exists(key string) (bool, error)
	Get(key string) (interface{}, error)
	Del(key string) error
}

// FileSystem interface for allowing swap outs for testing
type FileSystem interface {
	Put(path string, name string, payload []byte) error
	Get(path string, name string) ([]byte, error)
	Del(path string, name string) error
}

// Ddb interface for allowing swap outs for testing
type Ddb interface {
	CreateTable(input *dynamodb.CreateTableInput) error
	ListTables() ([]string, error)
	Put(tableName string, item interface{}) error
	PutConditional(tableName string, item interface{}, condition *string, conditionAttributes map[string]interface{}) error
	Get(tableName string, key map[string]string) (map[string]types.AttributeValue, error)
	Del(tableName string, key map[string]string) (error)
	Query(queryInput *dynamodb.QueryInput) ([]map[string]types.AttributeValue, error)
	Scan(tableName string, filter expression.ConditionBuilder) ([]map[string]types.AttributeValue, error)
}

// AppRes interface for allowing transactions
type AppRes interface {
	Begin() error
	Commit() error
	Rollback() error
}

func verboseLog(msg string) {
	if os.Getenv("GSBVERBOSE") != "" {
		log.Println("GSB.AppRes. %s", msg)
	}
}
