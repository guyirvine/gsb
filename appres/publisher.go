package appres

import (
	"log"
	"net/url"
)

type appResPublisher interface {
	Name() string
	Publish(payload string) error
	commit() error
}

// Publisher is the generic interface to a Publisher
type Publisher interface {
	Name() string
	Publish(payload string) error
	Commit() error
}

// NewPublisher allows Publishers to be created and returned, based on the url string provided.
func NewPublisher(urlString string) Publisher {
	urlObj, err := url.Parse(urlString)
	if err != nil {
		log.Printf("Error parsing URL for queue. Environment Variable: %s\n", urlString)
		log.Fatal(err)
	}

	var publisher Publisher
	switch urlObj.Scheme {
	case "sns":
		publisher = NewPublisherSns(urlObj)
	case "inmemp":
		publisher = NewPublisherInMemory(urlObj)
	default:
		log.Fatalf("Publisher type not recognised. Environment Variable: %s\n", urlString)
	}

	return publisher
}
