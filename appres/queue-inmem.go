package appres

import (
	"log"
	"net/url"
)

var (
	singleton map[string]*QueueInMemory
)

// QueueInMemory swap in queue for testing
type QueueInMemory struct {
	url  *url.URL
	name string
	List []string
}

// ResetQueueInMemory reset the singleton
func ResetQueueInMemory() {
	singleton = nil
}

// NewQueueInMemory returns the singleton, creating if necessary
func NewQueueInMemory(url *url.URL) *QueueInMemory {
	if singleton == nil {
		singleton = make(map[string]*QueueInMemory)
	}
	name := url.Path[1:]

	if _, exists := singleton[name]; !exists {
		singleton[name] = new(QueueInMemory)
		singleton[name].url = url
		singleton[name].name = url.Path[1:]
	}

	return singleton[name]
}

// NewQueueInMemoryFromString adapter for NewQueueInMemory
func NewQueueInMemoryFromString(urlString string) *QueueInMemory {
	urlObj, err := url.Parse(urlString)
	if err != nil {
		log.Printf("Error parsing URL for queue. urlString: %s\n", urlString)
		log.Fatal(err)
	}

	return NewQueueInMemory(urlObj)
}

// Init wrapper for InMemory Queue
func (mq *QueueInMemory) Init() error {
	return nil
}

// Name wrapper for InMemory Queue
func (mq *QueueInMemory) Name() string {
	return mq.name
}

// Retrieve wrapper for InMemory Queue
func (mq *QueueInMemory) Retrieve() (*string, error) {
	if len(mq.List) == 0 {
		return nil, nil
	}

	payload := &mq.List[0]
	mq.List = mq.List[1:]
	return payload, nil
}

// Send wrapper for InMemory Queue
func (mq *QueueInMemory) Send(payload string) error {
	mq.List = append(mq.List, payload)

	return nil
}

// Move message to a different queue
func (mq *QueueInMemory) Move(fromS string, toS string) {
	fromQ := singleton[fromS]

	payload := fromQ.List[0]
	fromQ.List = fromQ.List[1:]

	toQ := singleton[toS]
	toQ.List = append(toQ.List, payload)
}

// Begin transaction
func (mq *QueueInMemory) Begin() error {
	return nil
}

// Commit transaction
func (mq *QueueInMemory) Commit() error {
	if len(mq.List) > 0 {
		mq.List = mq.List[1:]
	}

	return nil
}

// Rollback transaction
func (mq *QueueInMemory) Rollback() error {
	return nil
}
