package gsb

import (
	"net/url"

	"gitlab.com/guyirvine/gsb/appres"
)

type monitorQueue struct {
	url *url.URL
	Q   appres.Queue
}

func newMonitorQueue(url *url.URL) *monitorQueue {
	sqs := new(monitorQueue)
	sqs.url = url

	return sqs
}

func (q *monitorQueue) init() error {
	q.Q = appres.NewQueue(q.url.String())

	return nil
}

func (q *monitorQueue) next() (*MonitorData, error) {
	payload, err := q.Q.Retrieve()
	if err != nil {
		return nil, err
	}

	if payload == nil && err == nil {
		return nil, nil
	}

	m := MonitorData{q.Q.Name(), *payload}

	return &m, nil
}

func (q *monitorQueue) commit() error {
	return q.Q.Commit()
}
