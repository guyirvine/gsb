package gsb

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/guyirvine/gsb/appres"
)

// Host is the root of GSB
type Host struct {
	Name           string
	Mq             mq
	MqURL          *url.URL
	LocalSendQ     appres.Queue
	AuditQ         appres.Queue
	ErrorQ         appres.Queue
	Handler        map[string]*handlerWrapper
	appResources   *appResources
	monitors       *monitors
	crons					 *crons
	maxRetries     int
	gsbVerboseEnabled bool
	verboseEnabled bool
	auditEnabled   bool
	loopCount      int
}

type messageSender interface {
	Send(string, string) error
}

func (h *Host) verboseLog(msg string) {
	if h.gsbVerboseEnabled {
		log.Printf("GSB. %s\n", msg)
	}
}

// AppVerboseLog provides user space verbose logging
func (h *Host) AppVerboseLog(msg string) {
	if h.verboseEnabled {
		log.Printf("App. %s\n", msg)
	}
}

func (h *Host) setName() {
	h.Name = os.Getenv("NAME")
	if h.Name != "" {
		return
	}

	path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	parts := strings.Split(path, "/")
	h.Name = parts[len(parts)-1]

	log.Printf("Host Name: %s\n", h.Name)
}

func (h *Host) setRetries() {
	if os.Getenv("RETRIES") == "" {
		h.maxRetries = 3
		return
	}

	var err error
	h.maxRetries, err = strconv.Atoi(os.Getenv("RETRIES"))
	if err != nil {
		log.Fatal(err)
	}
	if h.maxRetries < 0 {
		h.maxRetries = 0
	}
}

func (h *Host) initErrorQueue() {
	errorQueueName := os.Getenv("ERROR_Q")
	if errorQueueName == "" {
		errorQueueName = "error"
	}

	errorURL := h.MqURL
	errorURL.Path = "/" + errorQueueName

	h.ErrorQ = appres.NewQueue(errorURL.String())
}

func (h *Host) initAuditQueue() {
	auditQueueURL := os.Getenv("AUDIT_Q")
	if auditQueueURL == "" {
		h.auditEnabled = false
		h.AuditQ = nil
		return
	}
	h.auditEnabled = true

	h.AuditQ = appres.NewQueue(auditQueueURL)
}

func (h *Host) setverboseLogging() {
	h.gsbVerboseEnabled = (os.Getenv("GSBVERBOSE") != "")
	log.Printf("GSB Verbose Logging On: %v\n", h.gsbVerboseEnabled)

	h.verboseEnabled = (os.Getenv("VERBOSE") != "")
	log.Printf("Verbose Logging On: %v\n", h.verboseEnabled)
}

func (h *Host) setLoopCount() {
	h.loopCount = 7
	count := os.Getenv("LOOPCOUNT")
	if count != "" {
		loopCount, err := strconv.Atoi(count)
		if err != nil {
			log.Printf("Environment Variable LOOPCOUNT must be a number. Actual: %s\n", count)
		}
		h.loopCount = loopCount
	}
	log.Printf("Loop Count: %v\n", h.loopCount)
}


func (h *Host) init() {
	h.setName()
	h.initMq()
	h.initErrorQueue()
	h.initAuditQueue()
	h.setRetries()
	h.setverboseLogging()
	h.setLoopCount()

	h.Handler = make(map[string]*handlerWrapper)

	h.appResources = newAppResources()
	h.monitors = newMonitors(h)
	h.crons = newCrons(h)
}

// NewHost creates, configures and returns a host
func NewHost() *Host {
	host := new(Host)
	host.init()

	return host
}

func (h *Host) processError(env *envelope, err error) {
	env.AddError(err.Error())
	h.verboseLog(fmt.Sprintf("Envelope: %v", env))
	sendErr := h.sendEnvelope(h.ErrorQ, *env)
	if sendErr != nil {
		log.Printf("Error processing message: %v, \n", err)
		log.Printf("Error processing error\n")
		log.Fatal(sendErr)
	}
}

func (h *Host) processMessage(env *envelope) {
	var err error

	for i := 0; i <= h.maxRetries; i++ {
		if i > 0 {
			log.Printf("processMessage about to be retried for Message: %s. Attempt %d of %d.", env.Name, i, h.maxRetries)
		}
		err = h.processHandlers(env)
		if err == nil {
			return
		}
	}

	if err != nil {
		log.Printf("Could not process Message: %s. Moving to error queue", env.Name)
		h.processError(env, err)
	}
}

func (h *Host) processCommand(cmd *string) {
	switch *cmd {
	case "GSB_VERBOSE_ON":
		h.verboseEnabled = true
	case "GSB_VERBOSE_OFF":
		h.verboseEnabled = false
	case "GSB_AUDIT_ON":
		if h.AuditQ != nil {
			h.auditEnabled = true
		} else {
			log.Printf("No Audit Queue configured, so cannot enable Audit.")
			log.Printf("AUDIT_Q=<type>://<host>/<queueName>")
		}
	case "GSB_AUDIT_OFF":
		h.auditEnabled = false
	}
}

// Tick initiatae a single pass
func (h *Host) Tick() {
	// TODO add env var for loop count
	for i := 0; i < h.loopCount; i++ {
		payload := h.mqRetrieve()
		if payload != nil {
			h.verboseLog("Tick.1. payload: " + *payload)
			if len(*payload) > 4 && (*payload)[:4] == "GSB_" {
				h.processCommand(payload)
				h.sendCommandToAudit(*payload)
				h.mqCommit()
			} else {
				env, err := unmarshalEnvelope([]byte(*payload))
				if err != nil {
					log.Fatalf("Could not Unmarshal envelope. %s\n", *payload)
				}

				if env != nil {
					h.processMessage(env)
					h.sendToAudit(*env)
					h.mqCommit()
				}
			}
		} else {
			h.verboseLog("Tick.1.")
			break
		}
	}

	h.verboseLog("Tick.2.")

	h.monitors.process()

	h.verboseLog("Tick.3.")

	// Calling Sleep method
	time.Sleep(10 * time.Millisecond)
	h.verboseLog("Tick.4.")
}

func (h *Host) sendToAudit(env envelope) {
	if h.auditEnabled == true {
		h.sendEnvelope(h.AuditQ, env)
	}
}

func (h *Host) sendCommandToAudit(payload string) {
	if h.auditEnabled == true {
		h.AuditQ.Send(payload)
	}
}

func (h *Host) sendEnvelope(q appres.Queue, env envelope) error {
	payload, err := env.Marshal()
	if err != nil {
		return err
	}

	q.Send(string(payload))

	return nil
}

// SendMessage wraps a message in an envelope and puts it to the passed in queue
func (h *Host) SendMessage(q appres.Queue, name string, payload string) error {
	env, err := newEnvelope(q.Name(), name, payload)
	if err != nil {
		return err
	}

	err = h.sendEnvelope(q, env)
	if err != nil {
		return err
	}

	return nil
}

// Send is the channel for user space code to send a message with GSB
func (h *Host) Send(name string, payload string) error {
	h.verboseLog("gsb.Send.1 ")
	return h.SendMessage(h.LocalSendQ, name, payload)
}

// Run is called from user space after configuration
func (h *Host) Run() {
	startHTTPServer()
	h.crons.Start()

	log.Printf("Ready\n")

	for {
		h.Tick()
	}

	h.crons.Stop()
}
