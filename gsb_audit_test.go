package gsb

import (
	"os"
	"testing"

	"gitlab.com/guyirvine/gsb/appres"
)

type CmdsHandler struct {
}

func (h *CmdsHandler) Name() string {
	return "CmdsHandler"
}

func (h *CmdsHandler) Handle(_ string) error {
	return nil
}

func setupForAudit() *Host {
	ResetMqInMemory()
	appres.ResetQueueInMemory()

	os.Setenv("NAME", "TESTING")
	os.Setenv("MQ", "inmem://localhost/test")
	os.Setenv("AUDIT_Q", "inmem://localhost/audit")
	os.Setenv("RETRIES", "0")

	h := NewHost()
	h.AddHandler(new(CmdsHandler))

	return h
}

func TestAudit(t *testing.T) {
	h := setupForAudit()

	testQ := NewMqInMemoryFromString(os.Getenv("MQ"))
	auditQ := appres.NewQueueInMemoryFromString("inmem://localhost/audit")
	if len(auditQ.List) != 0 {
		t.Fatalf("TestAudit. len(auditQ.List). expected: 0, actual: %d", len(auditQ.List))
	}

	h.Send("Cmds", "Test Message 1")
	h.Tick()
	if len(auditQ.List) != 1 {
		t.Fatalf("TestAudit. len(auditQ.List). expected: 1, actual: %d", len(auditQ.List))
	}

	testQ.Send("GSB_AUDIT_OFF")
	h.Tick()
	h.Send("Cmds", "Test Message 2")
	h.Tick()
	if len(auditQ.List) != 1 {
		t.Fatalf("TestAudit. len(auditQ.List). expected: 1, actual: %d", len(auditQ.List))
	}

	testQ.Send("GSB_AUDIT_ON")
	h.Tick()
	if len(auditQ.List) != 2 {
		t.Fatalf("TestAudit. len(auditQ.List). expected: 2, actual: %d", len(auditQ.List))
	}

	h.Send("Cmds", "Test Message 3")
	h.Tick()
	if len(auditQ.List) != 3 {
		t.Fatalf("TestAudit. len(auditQ.List). expected: 3, actual: %d", len(auditQ.List))
	}
}
